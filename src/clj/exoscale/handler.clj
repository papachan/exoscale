(ns exoscale.handler
  (:require
   [reitit.ring :as reitit-ring]
   [exoscale.middleware :refer [middleware]]
   [hiccup.page :refer [include-js include-css html5]]
   [config.core :refer [env]]))

(def mount-target
  [:div#app
   [:p "please wait while Figwheel/shadow-cljs is waking up ..."]
   [:p "(Check the js console for hints if nothing exciting happens.)"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1.0"}]
   [:link {:href "https://fonts.googleapis.com/icon?family=Material+Icons" :rel "stylesheet"}]
   (include-css (if (env :dev) "/css/styles.css" "/css/styles.min.css"))])

(defn loading-page []
  (html5
   (head)
   [:body {:class "body-container"}
    mount-target
    (include-js "/js/app.js")
    [:script "exoscale.core.init_BANG_()"]]))

(defn index-handler
  [_request]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (loading-page)})

(def router
  (reitit-ring/router
    [["/" {:get {:handler index-handler}}]
     ["/items" {:get {:handler index-handler}}]
     ["/items-page-2" {:get {:handler index-handler}}]
     ["/items-page-3" {:get {:handler index-handler}}]
     ["/cart" {:get {:handler index-handler}}]]))

(def app
  (reitit-ring/ring-handler
   router
   (reitit-ring/routes
    (reitit-ring/create-resource-handler {:path "/" :root "/public"})
    (reitit-ring/create-default-handler))
   {:middleware middleware}))
