(ns exoscale.page
  (:require
   [exoscale.components :refer [top-bar
                                home-page
                                items-page
                                items-page-2
                                items-page-3
                                cart-page]]
   [exoscale.config :refer [products
                            currency]]
   [reagent.session :as session]))

;; -------------------------
;; Page mounting component

(defn current-page []
  (fn []
    (let [data nil
          page (:current-page (session/get :route))]
      ;; (print (session/get :route))
      [:div
       [top-bar]
       [:div.container [page @products @currency]]])))


;; -------------------------
;; Translate routes -> page components

(defn page-for [route]
  (case route
    :index #'home-page
    :items #'items-page
    :items-page-2 #'items-page-2
    :items-page-3 #'items-page-3
    :cart #'cart-page))
