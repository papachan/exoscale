(ns exoscale.components
  (:require [exoscale.util :refer [key->str
                                   price-formatter]]
            [accountant.core :as accountant]
            [reagent.cookies :as cookies]
            [exoscale.route :as route :refer [path-for]]
            [exoscale.config :refer [currency
                                     items
                                     products
                                     cart
                                     save-cart!]]))


;; -------------------------
;; Page components
(defn top-bar
  []
  (let [page-title "Cloud Store"]
    [:div.app-top-bar
     [:h1 [:a {:href (path-for :index)} page-title]]
     [:a {:href (path-for :cart)
          :class "fancy-button button"}
      [:i.material-icons "shopping_cart"]
      "Checkout"]]))

(defn home-page
  []
  (fn []
    [:span.main
     [:h2 "Welcome"]
     [:div
      [:ul
       [:li [:a {:href (path-for :items)} "Servers list"]]]]]))

(defn items-page
  [hdata currency]
  (fn []
    (letfn [(handle-click!
              [e]
              (let [n (.-name (.-target e))]
                (swap! items assoc-in [:items 0] {(keyword n) (get (:items hdata) (keyword n))})
                (save-cart!)
                (accountant/navigate! (path-for :items-page-2))))]
      [:div
       [:p [:a {:href (path-for :index)} "<< Back to home"]]
       [:ul
        [:li
         (for [[k v] (:items hdata)] ^{:key k}
           [:div
            [:h3 (key->str k)]
            [:p
             [:strong "Price: " (price-formatter v (.toUpperCase (name currency)))]]
            [:button {:onClick handle-click!
                      :name (name k)} "buy"]])
         ]]
       ])))

(defn items-page-2
  [hdata currency]
  (fn []
    (letfn [(handle-click!
              [e]
              (let [n (.-name (.-target e))]
                (swap! items assoc-in [:items 1] {(keyword n) (get (:options hdata) (keyword n))})
                (save-cart!)
                (accountant/navigate! (path-for :items-page-3))))]
      [:div
       [:p [:a {:href "#"
                :on-click #(.back js/window.history)} "<< Back"]]
       [:ul
        [:li
         (for [[k v] (:options hdata)] ^{:key k}
           [:div {:data (name k)}
            [:h3 (key->str k)
             [:p
              [:strong "Price: " (price-formatter v currency)]]
             [:button {:onClick handle-click!
                       :name (name k)} "buy"]
             ]])]]
       ])))

(defn items-page-3
  [hdata currency]
  (fn []
    (letfn [(handle-click!
              [e]
              (let [n (.-name (.-target e))]
                (do
                  (swap! items assoc-in [:items 2] {(keyword n) (get (:storage hdata) (keyword n))})
                  (save-cart!)
                  (accountant/navigate! (path-for :cart)))))]
      [:div
       [:p [:a {:href "#"
                :on-click #(.back js/window.history)} "<< Back"]]
       [:ul
        [:li
         (for [[k v] (:storage hdata)] ^{:key k}
           [:div {:data (name k)}
            [:h3 (key->str k)
             [:p
              [:strong "Price: " (price-formatter v currency)]]
             [:button {:onClick handle-click!
                       :name (name k)} "buy"]
             ]])]]
       ])))


(defn cart-page
  [hdata currency]
  (fn []
    (let [data (if (nil? @cart)
                 (cookies/get :exoscale.io)
                 @cart)
          values (map val (:items data))
          v (map #(.parseFloat js/window %) values)
          total (apply + v)]
      [:div {:class "cart"}
       [:p [:a {:href "#"
                :on-click #(.back js/window.history)} "<< Back"]]
       [:h2 "Your shopping cart:"]
       [:ul
        [:li
         (for [[k v] (:items data)] ^{:key k}
           [:div
            [:div {:class "items"} (key->str (name k))]
            [:div {:class "price-items"}
              "Price: " (price-formatter v currency)]
            ])]]
       [:div {:class "checkout"} "Total checkout: " (price-formatter total currency)]
       [:button {:onClick
                 (do
                   (cookies/clear!)
                   (reset! cart nil)
                   #(accountant/navigate! (path-for :index)))} "finalize"]])))
