(ns exoscale.util
  (:require [clojure.string :refer [capitalize
                                    replace]]
            [goog.string :refer [format]]))

(defn key->str
  [n]
  (capitalize
   (replace (name n) #"_" \space)))

(defn price-formatter
  [val curr]
  (format "%.2f %s" val (.toUpperCase (name curr))))
