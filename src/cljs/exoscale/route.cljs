(ns exoscale.route
  (:require [reagent.core :as reagent :refer [atom]]
            [reitit.frontend :as reitit]))

(def router
  (reitit/router
   [["/" :index]
    ["/items"
     ["" :items]]
    ["/items-page-2"
     ["" :items-page-2]]
    ["/items-page-3"
     ["" :items-page-3]]
    ["/cart"
     ["" :cart]]
    ]))

(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))
