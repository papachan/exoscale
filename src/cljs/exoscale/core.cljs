(ns exoscale.core
  (:require [exoscale.config :refer [base-uri
                                     urls
                                     currency]]
            [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [reitit.frontend :as reitit]
            [clerk.core :as clerk]
            [accountant.core :as accountant]
            [exoscale.route :refer [router path-for]]
            [exoscale.components :as comp :refer [top-bar]]
            [exoscale.page :refer [page-for current-page]]
            [exoscale.http :refer [http-call]]
            [cljs.core.async :refer [<! >! timeout take! chan put!] :refer-macros [go]]
            [clojure.string :refer [includes?]]
            [exoscale.config :refer [set-map-list!]]))


(enable-console-print!)

;; -------------------------
;; Initialize app

(defn mount-root []
  (reagent/render
   [current-page] (.getElementById js/document "app")))

(defn load-urls
  []
  (let [c (chan)
        curr (if (nil? @currency) :chf @currency)]
    (doseq [url (mapv #(str base-uri %) urls)]
      (http-call url c)
      (take! c (fn [x]
                 (let [body (get (:response x) curr)
                       url (:url x)]
                   (cond
                     (includes? url "opencompute.json") (set-map-list! body :items)
                     (includes? url "sos.json") (set-map-list! body :storage)
                     (includes? url "licenses.json") (do (set-map-list! body :options)
                                                          (mount-root)))))))))

(defn ^:export init!
  []
  (clerk/initialize!)
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [match (reitit/match-by-path router path)
            current-page (:name (:data  match))
            route-params (:path-params match)]
        (reagent/after-render clerk/after-render!)
        (session/put! :route {:current-page (page-for current-page)
                              :route-params route-params})
        (clerk/navigate-page! path)))
    :path-exists?
    (fn [path]
      (boolean (reitit/match-by-path router path)))})
  (accountant/dispatch-current!)
  ;; load data when page initialize
  (load-urls))
