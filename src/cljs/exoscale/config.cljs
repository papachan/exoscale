(ns exoscale.config
  (:require [reagent.cookies :as cookies]))

;; ------------------------------------------------------------
;; Urls to fetch data
(def base-uri "https://sos-de-muc-1.exo.io/exercises-pricing-data/")

(def urls ["licenses.json"
           "opencompute.json"
           "sos.json"])

;; ------------------------------------------------------------
;; From cart items
(def cart (atom nil))

(def items (atom {:items []}))

;; ------------------------------------------------------------
;; Data atom
(def products (atom {}))

(defn set-map-list!
  [val key]
  (swap! products assoc-in [key] val))

(defn save-cart!
  []
  (let [v (:items @cart)]
    (swap! cart assoc-in [:items] (merge v (into {} (:items @items))))
    (cookies/clear!)
    (.set goog.net.cookies "exoscale.io" @cart)))

;; ------------------------------------------------------------
;; Currency atom
;; We should setup currency from an URL parameters
(defonce chf :chf)
(defonce eur :eur)
(def currency (atom chf))
