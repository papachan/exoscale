(ns exoscale.http
  (:require [cljs.core.async :refer [<! >! timeout take! chan put!]]
            [cljs-http.client :as http])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))


;; ------------------------------------------------------------
(defn http-call
  [url channel]
  (when (not (empty? url))
    (go (let [res (<! (http/get url {:with-credentials? false}))]
        (if (= (:status res) 200)
          (do
            (>! channel {:response (:body res) :url url}))
          (throw (js/Error. "http error")))))))
