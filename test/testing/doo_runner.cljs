(ns testing.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [testing.core-test]))

(doo-tests 'testing.core-test)
