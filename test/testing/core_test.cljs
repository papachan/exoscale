(ns testing.core-test
  (:require [cljs.test :refer-macros [deftest is are testing run-tests async use-fixtures]]
            [reagent.core :as reagent :refer [atom]]
            [cljs.core.async :refer [<! >! timeout take! chan]]
            [cljs-http.client :as http]
            [ajax.core :refer [GET]]
            [clojure.string :refer [includes?]]
            [exoscale.components :refer [top-bar]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(use-fixtures
  :each
  {:before (fn [] (println "Setting up tests..."))
   :after (fn [] (println "Tearing down tests..."))})

(deftest test-dummy
  (is (= 2 2)))
