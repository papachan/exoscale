(ns exoscale.prod
  (:require [exoscale.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
