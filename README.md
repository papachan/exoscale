# Cloud Store

Your cloud store site with reagent and clojurescript.

## Development mode

### start shadow-cjs

To start the Figwheel compiler use this command:

```
   lein run -m shadow.cljs.devtools.cli --npm watch app
```

Or:

```
   shadow-cljs watch app
```

The application will now be available at:

[http://localhost:3000](http://localhost:3000).



### Run tests with Karma:

Fetch modules from node:

```
    npm install
```

Run this command:

```
    shadow-cljs compile test && karma start --single-run
```
